package lex.hack.levels;

import lex.hack.game.Game;
import lex.hack.game.Scene;
import lex.hack.game.blocks.Block;
import lex.hack.game.blocks.Blocks;
import lex.hack.game.entities.Enemy;

public class TestLevel extends Scene {

	public TestLevel(Game game, int starty) {
		super(game, starty, 10);
	}
	
	@Override
	public Block initializeBlock(int x, int y, int z) {
		if(z == 0) {
			if(Math.random() < 0.2) {
				return Blocks.STONE;
			}
			else if(Math.random() < (0.01 + (starty + y) * 0.0001)) {
				game.entities.add(new Enemy(game, x, y + starty, 100, 0.5));
				return Blocks.GRASS;
			}
			else if(z == 0) {
				return Blocks.GRASS;
			}
		}
		
		return null;
	}
	
}
