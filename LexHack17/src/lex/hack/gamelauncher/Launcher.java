package lex.hack.gamelauncher;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import lex.hack.game.Main;

public class Launcher {
	public static JFrame frame;
	public static JPanel panel;
	
	public static JLabel label1;
	public static JSlider slider1;
	public static JSlider slider2;
	public static JSlider slider3;
	public static JSlider slider4;
	public static JButton startbutton;
	
	public static String GAME_NAME = "Game Launcher";
	public static String VERSION = "1.0";
	public static String GAME_DESC = "Click \"Start\" to start the game.";
	
	public static void main(String[] args) {
		// Initialize game window
		
		frame = new JFrame(GAME_NAME);
		frame.setPreferredSize(new Dimension(300, 350));
		frame.setSize(300, 350);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.add(panel = new JPanel());
		BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
		panel.setLayout(layout);
		panel.setPreferredSize(new Dimension(300, 350));
		
		// Populate panel with buttons
		panel.add(label1 = new JLabel(GAME_DESC));
		panel.setAlignmentX(0.5f);
		
		panel.add(new JLabel("Difficulty Level"));
		panel.add(slider1 = new JSlider(JSlider.HORIZONTAL, 1, 10, 5));
		slider1.setMajorTickSpacing(9);
		slider1.setMajorTickSpacing(1);
		slider1.setPaintTicks(true);
		slider1.setPaintLabels(true);
		panel.add(new JLabel("Enemy Level Offset"));
		panel.add(slider2 = new JSlider(JSlider.HORIZONTAL, 0, 20, 0));
		slider2.setMajorTickSpacing(10);
		slider2.setMinorTickSpacing(1);
		slider2.setPaintTicks(true);
		slider2.setPaintLabels(true);
		panel.add(new JLabel("Enemy Level Growth Per 1,000 Units"));
		panel.add(slider3 = new JSlider(JSlider.HORIZONTAL, 10, 30, 15));
		slider3.setMajorTickSpacing(10);
		slider3.setMinorTickSpacing(1);
		slider3.setPaintTicks(true);
		slider3.setPaintLabels(true);
		panel.add(new JLabel("Enemy Level Growth Per Hour"));
		panel.add(slider4 = new JSlider(JSlider.HORIZONTAL, 10, 30, 15));
		slider4.setMajorTickSpacing(10);
		slider4.setMinorTickSpacing(1);
		slider4.setPaintTicks(true);
		slider4.setPaintLabels(true);
		
		panel.add(startbutton = new JButton("Start"));
		startbutton.setAlignmentX(0.5f);
		startbutton.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	Main.settingsAIDiff = slider1.getValue();
		    	Main.settingsLevelOffset = slider2.getValue();
		    	Main.settingsDistGrowth = slider3.getValue();
		    	Main.settingsTimeGrowth = slider4.getValue();
		        Main.main();
		    }
		});
		
		frame.setVisible(true);
	}
}
