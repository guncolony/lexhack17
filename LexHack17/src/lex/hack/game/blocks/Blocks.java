package lex.hack.game.blocks;

import java.awt.Color;

import lex.hack.game.Main;

/**
 * A class with some preset blocks
 * @author Jason
 *
 */
public final class Blocks {
	public static final Block EMPTY = new Block().setName("Empty").setCollideable(true);
	
	public static final Block STONE = new Block().setName("Stone").setImage(Main.stoneImage).setCollideable(true);
	public static final Block DIRT = new Block().setName("Dirt").setColor(new Color(100, 140, 40));
	public static final Block GRASS = new Block().setName("Grass").setImage(Main.grassImage);
	public static final Block LAVA = new Block().setName("Lava").setColor(new Color(255, 204, 0));
	
}
