package lex.hack.game.blocks;

import java.awt.Color;

import processing.core.PImage;

/**
 * A 2d block taking 1x1 space in a scene.
 * @author Jason
 *
 */
public class Block {
	
	public String name = "Block";
	public PImage image = null;
	public Color color = null;
	
	public boolean collideable = false;
	
	public Block() {
		
	}
	
	public Block setName(String name) {
		this.name = name;
		return this;
	}
	
	public Block setImage(PImage image) {
		this.image = image;
		return this;
	}
	
	public Block setColor(Color color) {
		this.color = color;
		return this;
	}

	public Block setCollideable(boolean collide) {
		this.collideable = collide;
		return this;
	}
	
	public PImage getImage() {
		return image;
	}
	public Color getColor() {
		return color;
	}
	
	public String toString() {
		return name;
	}
}
