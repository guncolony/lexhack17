package lex.hack.game;

import lex.hack.gamelauncher.Launcher;
import processing.core.PApplet;
import processing.core.PImage;

public class Main extends PApplet {
	
	// Settings
	public static int settingsAIDiff = 0;
	public static int settingsLevelOffset = 0;
	public static int settingsDistGrowth = 0;
	public static int settingsTimeGrowth = 0;
	
	// Input
	public boolean W = false, A = false, S = false, D = false;
	
	public static PImage grassImage;
	public static PImage stoneImage;
	

	public static PImage playerImage;
	public static PImage zombieImage;

	/**
	 * Begin the game.
	 */
	public static void main() {
		PApplet.main("lex.hack.game.Main");
	}
	
	// State info
	Game currentGame = null;
	
	
	@Override
	public void settings() {
		size(1280, 720, FX2D);
	}
	
	/*
	@Override
	public void exit() {
		if (looping) {
	      // dispose() will be called as the thread exits
	      finished = true;

	    }
		surface.setVisible(false);
		Launcher.frame.setVisible(true);
	}
	*/
	
	@Override
	public void setup() {
		surface.setResizable(true);
        fill(120,50,240);
        
        grassImage = loadImage("grassBlock2.png");
        stoneImage = loadImage("stoneBlock.png");
        playerImage = loadImage("guncharacter.png");
        zombieImage = loadImage("mob01elite.png");
		
		currentGame = new Game(this);
	}
	
	@Override
	public void draw() {
		background(0,0,0);
		if(currentGame == null) {
			text("Game is not running.", 640f, 360f);
		}
		else {
			currentGame.draw();
			// Master overlays
			fill(255,255,255);
			text("FPS: " + (int) Math.round(frameRate), 50,50);
		}
	}
	
	@Override
	public void keyPressed() {
		if(key == 'W') {
			W = true;
		}
		else if(key == 'A') {
			A = true;
		}
		else if(key == 'S') {
			S = true;
		}
		else if(key == 'D') {
			D = true;
		}
	}
	
	@Override
	public void keyReleased() {
		if(key == 'W') {
			W = false;
		}
		else if(key == 'A') {
			A = false;
		}
		else if(key == 'S') {
			S = false;
		}
		else if(key == 'D') {
			D = false;
		}
	}
	
	// Scale
	public double screenScale() {
		return Math.min(width / 1280.0, height / 720.0);
	}
	
	public double xEdge() {
		return (width - screenScale() * 1280) / 2;
	}
	
	public double yEdge() {
		return (height - screenScale() * 720) / 2;
	}
	
	public float scrX(double x) {
		return (float) (xEdge() + x * screenScale());
	}
	public float scrY(double y) {
		return (float) (height - (yEdge() + y * screenScale()));
	}
	public float scr(double d) {
		return (float) (d * screenScale());
	}
	public float gX(double x) {
		return (float) ((x - xEdge()) / screenScale());
	}
	public float gY(double y) {
		return (float) ((height - y - yEdge()) / screenScale());
	}
	public float g(double d) {
		return (float) (d / screenScale());
	}
}
