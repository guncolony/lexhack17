package lex.hack.game.astar;

import java.util.List;

/**
  The AStarNode class, along with the AStarSearch class,
  implements a generic A* search algorithm. The AStarNode
  class should be subclassed to provide searching capability.
  http://www.peachpit.com/articles/article.aspx?p=101142&seqNum=2
*/
public abstract class AStarNode implements Comparable<Object> {

  //AStarNode pathParent;
  //float costFromStart;
  //float estimatedCostToGoal;


  public float getCost() {
    return AStarSearch.active.costFromStart.get(this) + AStarSearch.active.estimatedCostToGoal.get(this);
  }


  public int compareTo(Object other) {
    float thisValue = this.getCost();
    float otherValue = ((AStarNode)other).getCost();

    float v = thisValue - otherValue;
    return (v>0)?10:(v<0)?-10:0; // sign function
  }


  /**
    Gets the cost between this node and the specified
    adjacent (AKA "neighbor" or "child") node.
  */
  public abstract float getCost(AStarNode node);


  /**
    Gets the estimated cost between this node and the
    specified node. The estimated cost should never exceed
    the true cost. The better the estimate, the more
    effecient the search.
  */
  public abstract float getEstimatedCost(AStarNode node);


  /**
    Gets the children (AKA "neighbors" or "adjacent nodes")
    of this node.
  */
  public abstract List getNeighbors();
}  