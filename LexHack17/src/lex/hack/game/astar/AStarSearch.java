package lex.hack.game.astar;

import java.util.*;

/**
  The AStarSearch class, along with the AStarNode class,
  implements a generic A* search algorithm. The AStarNode
  class should be subclassed to provide searching capability.
  http://www.peachpit.com/articles/article.aspx?p=101142&seqNum=2
*/
public class AStarSearch {
	
	public static AStarSearch active;
	
	public HashMap<AStarNode, AStarNode> pathparent = new HashMap<>();
	public HashMap<AStarNode, Float> costFromStart = new HashMap<>();
	public HashMap<AStarNode, Float> estimatedCostToGoal = new HashMap<>();
	
	public AStarSearch() {
		active = this;
	}

  /**
    A simple priority list, also called a priority queue.
    Objects in the list are ordered by their priority,
    determined by the object's Comparable interface.
    The highest priority item is first in the list.
  */
  public static class PriorityList extends LinkedList {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8220605605927541833L;

	public void add(Comparable object) {
      for (int i=0; i<size(); i++) {
        if (object.compareTo(get(i)) <= 0) {
          add(i, object);
          return;
        }
      }
      addLast(object);
    }
  }


  /**
    Construct the path, not including the start node.
  */
  protected LinkedList<AStarNode> constructPath(AStarNode node) {
    LinkedList<AStarNode> path = new LinkedList<>();
    while (pathparent.get(node) != null) {
      path.addFirst(node);
      node = pathparent.get(node);
    }
    return path;
  }


  /**
    Find the path from the start node to the end node. A list
    of AStarNodes is returned, or null if the path is not
    found. 
  */
  public LinkedList<AStarNode> findPath(AStarNode startNode, AStarNode goalNode) {

    PriorityList openList = new PriorityList();
    LinkedList closedList = new LinkedList();

    costFromStart.put(startNode, 0f);
    estimatedCostToGoal.put(startNode, 0f);
    pathparent.put(startNode, null);
    openList.add(startNode);

    while (!openList.isEmpty()) {
      AStarNode node = (AStarNode)openList.removeFirst();
      if (node.equals(goalNode)) {
        // construct the path from start to goal
        return constructPath(goalNode);
      }

      List neighbors = node.getNeighbors();
      for (int i=0; i<neighbors.size(); i++) {
        AStarNode neighborNode =
          (AStarNode)neighbors.get(i);
        boolean isOpen = openList.contains(neighborNode);
        boolean isClosed =
          closedList.contains(neighborNode);
        float costFromStart = this.costFromStart.get(node) +
          node.getCost(neighborNode);

        // check if the neighbor node has not been
        // traversed or if a shorter path to this
        // neighbor node is found.
        if ((!isOpen && !isClosed) ||
          costFromStart < this.costFromStart.get(neighborNode))
        {
          pathparent.put(neighborNode, node);
          this.costFromStart.put(neighborNode, costFromStart);
          this.estimatedCostToGoal.put(neighborNode, neighborNode.getEstimatedCost(goalNode));
          if (isClosed) {
            closedList.remove(neighborNode);
          }
          if (!isOpen) {
            openList.add(neighborNode);
          }
        }
      }
      closedList.add(node);
    }

    // no path found
    return null;
  }

}