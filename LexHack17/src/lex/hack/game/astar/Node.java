package lex.hack.game.astar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lex.hack.game.Game;
import lex.hack.game.blocks.Block;

public class Node extends AStarNode {
	
	public Game game;
	public int x;
	public int y;
	
	public Node(Game game, int x, int y) {
		this.game = game; this.x = x; this.y = y;
	}

	@Override
	public float getCost(AStarNode node) {
		return 1;
	}

	@Override
	public float getEstimatedCost(AStarNode node) {
		if(node instanceof Node) {
			Node n = (Node) node;
			return Math.abs(n.x - x) + Math.abs(n.y - y);
		}
		return 0;
	}

	@Override
	public List getNeighbors() {
		ArrayList<Node> neighbors = new ArrayList<Node>();
		int[] xs = new int[] {-1,0,0,1};
		int[] ys = new int[] {0,-1,1,0};
		for(int i = 0; i < 4; i++) {
			int coordx = x + xs[i]; int coordy = y + ys[i];
			if(!(game.isSolid(coordx, coordy))) {
				neighbors.add(new Node(game, coordx, coordy));
			}
		}
		return neighbors;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Node) {
			Node n = (Node) o;
			if(n.x == x && n.y == y) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return x + " " + y;
	}
	
	public int hashCode() {
		return x >> 8 + y;
	}

}
