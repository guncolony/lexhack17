package lex.hack.game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import lex.hack.game.blocks.Block;
import lex.hack.game.entities.Enemy;
import lex.hack.game.entities.Entity;
import lex.hack.game.entities.Player;
import lex.hack.levels.TestLevel;
import processing.core.PImage;

public class Game {
	public Main main;
	public Game(Main main) {
		this.main = main;
		
		// Initialize game
		scenes.add(new Scene(this, 0, 15));
		updateSceneData();
		
		entities.add(player = new Player(this, 15, 2, 100));
	}
	
	// Game state
	public LinkedList<Scene> scenes = new LinkedList<Scene>();
	public LinkedHashSet<Entity> entities = new LinkedHashSet<Entity>();
	public Player player;
	public int maxsceneend = 0;
	public int minsceneend = 0;
	
	// Methods
	public void draw() {
		main.text("Game is running.", 640f, 360f);
		// Tick

		while(maxsceneend - player.y <= 15) {
			scenes.add(new TestLevel(this, maxsceneend));
			updateSceneData();
			if(Math.random() < 0.5) {
				scenes.add(new Scene(this, maxsceneend, (int) (Math.random() * 10)));
			}
			updateSceneData();
		}
		while(player.y - minsceneend >= 15) {
			scenes.remove(0);
			updateSceneData();
		}

		long tickstart = System.currentTimeMillis();
		@SuppressWarnings("unchecked")
		HashSet<Entity> ents = (HashSet<Entity>) entities.clone();
		for(Entity ent : ents) {
			ent.tick();
			if(System.currentTimeMillis() - tickstart > 18) break;
		}
		
		// Render
		for(Scene scene : scenes) {
			scene.drawBlocks();
		}
		for(Entity ent : entities) {
			ent.render();
		}
		renderBlock(scrX(1.5), scrY(1.5), scr(1), Math.PI * 0.25, null, new Color(255, 255, 255));
		renderBlock(scrX(1.5), scrY(1.5), scr(1), 0, null, new Color(127, 255, 255));
	}
	
	public void updateSceneData() {
		Scene lastscene = scenes.get(scenes.size() - 1);
		maxsceneend = lastscene.starty + lastscene.height;
		Scene firstscene = scenes.get(0);
		minsceneend = firstscene.starty + firstscene.height;
	}
	
	
	// Coords helper
	public float scrX(double gamex) {
		return main.scrX(gamex * 40);
	}
	
	public float scrY(double gamey) {
		return main.scrY((gamey-player.y+9) * 40);
	}
	
	public float scr(double gamec) {
		return main.scr(gamec * 40);
	}
	
	public float gX(double x) {
		return main.gX(x) / 40;
	}

	public float gY(double y) {
		return main.gY(y) / 40 + (float) player.y - 9;
	}

	public float g(double c) {
		return main.g(c) / 40;
	}
	
	
	
	/**
	 * Get the pile of blocks at the specified location. If it is not part of any Scene, return an array of three Null blocks.
	 * @param x
	 * @param y
	 * @return
	 */
	public Block[] getBlocks(int x, int y) {
		for(Scene sc : scenes) {
			Block[] b = sc.getBlocks(x, y - sc.starty);
			if(b != null) return b;
		}
		return new Block[] {null, null, null};
	}
	
	/**
	 * Returns whether the position is unable to be passed through.
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isSolid(int x, int y) {
		Block[] blocks = getBlocks(x, y);
		return blocks[0] == null || blocks[0].collideable || (blocks[1] != null && blocks[1].collideable);
	}
	
	// Render helper
	/**
	 * Render a block or entity on the screen
	 * @param centerx Center X coords
	 * @param centery Center Y coords
	 * @param size Size of the block (the diameter)
	 * @param rotation Rotation of the image in radians
	 * @param image Image to draw (if any)
	 * @param color Color to fill in a square (if any)
	 */
	public void renderBlock(double centerx, double centery, double size, double rotation, PImage image, Color color) {
		
		if(centerx + size * 0.7 < 0 || centerx - size * 0.7 > main.width || centery + size * 0.7 < 0 || centery - size * 0.7 > main.height)
			return;
		
		// Apply rotation
		main.translate((float)centerx, (float)centery);
		if(rotation != 0) {
			main.rotate((float) rotation);
		}
		
		if(image != null) {
			// Draw image
			main.image(image, (float) -(size / 2), (float) -(size / 2), (float) size, (float) size);
		}
		if(color != null) {
			// Draw color
			main.stroke(color.getRGB());
			main.fill(color.getRGB());
			main.rect((float) -(size / 2), (float) -(size / 2), (float) size, (float) size);
		}
		
		// Revert rotation
		if(rotation != 0) {
			main.rotate(-(float) rotation);
		}
		main.translate(-(float)centerx, -(float)centery);
	}
}
