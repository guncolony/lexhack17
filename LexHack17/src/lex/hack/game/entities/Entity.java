package lex.hack.game.entities;

import lex.hack.game.Game;

public abstract class Entity {
	public Game game;
	public double x, y;
	
	public Entity(Game game, double x, double y) {
		this.game = game;
		this.x = x; this.y = y;
	}
	
	public abstract void tick();
	public abstract void render();
	
	
}
