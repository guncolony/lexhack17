package lex.hack.game.entities;

import java.util.LinkedList;
import java.util.List;

import lex.hack.game.Game;
import lex.hack.game.Main;
import lex.hack.game.astar.AStarNode;
import lex.hack.game.astar.AStarSearch;
import lex.hack.game.astar.Node;

public class Enemy extends EntityLiving{
	
	public double vx;
	public double vy;
	
	
    public Enemy(Game game, double x, double y, double hp, double size) {
		super(game, x, y, hp, size);
		
	}


	@Override
	public void tick() {
		// Drag velocity
		vx *= 0.7;
		vy *= 0.7;
		
		// Determine path to player
		Node thisnode = new Node(game, (int) x, (int) y);
		Player p = game.player;
		if(!game.isSolid((int) p.x, (int) p.y)) {

			Node pnode = new Node(game, (int) p.x, (int) p.y);
			
			LinkedList<AStarNode> nodelist = new AStarSearch().findPath(thisnode, pnode);
			
			if(nodelist != null && !nodelist.isEmpty()) {
				// Get the next node
				Node nextnode = (Node) nodelist.get(0);
			
				// Accelerate towards player
				double ax = nextnode.x - thisnode.x;
				double ay = nextnode.y - thisnode.y;
				vx += ax * 0.01;
				vy += ay * 0.01;
			}
			
			// Move
			x += vx;
			y += vy;
		}
	}

	@Override
	public void render() {
		double rotation = 0;
		double xx = vx;
		double yy = vy;
		if(vy != 0) {
			rotation = Math.atan(xx / yy);
			if(yy < 0) rotation += Math.PI;
		}
		game.renderBlock(game.scrX(x), game.scrY(y), game.scr(1.2), rotation, Main.zombieImage, null);
	}
}
