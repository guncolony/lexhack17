package lex.hack.game.entities;

import lex.hack.game.Game;
import lex.hack.game.Main;
import processing.core.PApplet;

public class Player extends EntityLiving {
	public double vx = 0, vy = 0;

	public Player(Game game, double x, double y, double hp) {
		super(game, x, y, hp, 0.4); // Hitbox size 0.4 tiles
	}

	@Override
	public void tick() {
		Main main = game.main;
		vx = vx * 0.85;
		vy = vy * 0.85;

		double ax = 0, ay = 0;
		if(main.W) ay += 1;
		if(main.A) ax -= 1;
		if(main.D) ax += 1;
		if(main.S) ay -= 1;
		
		if(ax != 0 || ay != 0) {
			double velocityInverse = 1 / Math.sqrt(ax* ax + ay * ay);
			ax *= velocityInverse * 0.009;
			ay *= velocityInverse * 0.009;
		}
		vx += ax;
		vy += ay;
		
		double nexty = y + vy;
		double nextx = x + vx;
		int intx = (int) Math.floor(x);
		int inty = (int) Math.floor(y);
		int intnextx = (int) Math.floor(nextx);
		int intnexty = (int) Math.floor(nexty);
		if(game.isSolid(intnextx, intnexty)) {
			if(!game.isSolid(intnextx, inty)) {
				x += vx;
			}
			else if(!game.isSolid(intx, intnexty)) {
				y += vy;
			}
		}
		else {
			// Check diagonals
			if(Math.abs(intnexty - inty) == 1 && Math.abs(intnextx - intx) == 1) {
				if(game.isSolid(intx, intnexty) && game.isSolid(intnextx, inty)) {
					// Don't move if the diagonal blocks are both solid
					return;
				}
			}
			x += vx;
			y += vy;
		}
		
		if(main.mousePressed) {
			if(main.mouseButton == PApplet.LEFT) {
				if(main.frameCount % 9 == 0) {
					double mouseX = game.gX(main.mouseX);
					double mouseY = game.gY(main.mouseY);
					double xx = mouseX - x;
					double yy = mouseY - y;
					double len = Math.sqrt(xx*xx+yy*yy) * 3;
					xx /= len;
					yy /= len;
					Projectile.fireBullet(game, this, x-yy, y+xx, mouseX, mouseY, 30);
					Projectile.fireBullet(game, this, x+yy, y-xx, mouseX, mouseY, 30);
				}
			}
		}
	}

	@Override
	public void render() {
		Main main = game.main;
		double rotation = 0;
		if(main.mousePressed) {
			double xx = game.gX(main.mouseX) - x;
			double yy = game.gY(main.mouseY) - y;
			rotation = Math.atan(xx / yy);
			if(yy < 0) rotation += Math.PI;
		}
		else {

			double xx = vx;
			double yy = vy;
			if(vy != 0) {
				rotation = Math.atan(xx / yy);
				if(yy < 0) rotation += Math.PI;
			}
		}
		game.renderBlock(game.scrX(x), game.scrY(y), game.scr(0.85), rotation, Main.playerImage, null);
	}
	
}
