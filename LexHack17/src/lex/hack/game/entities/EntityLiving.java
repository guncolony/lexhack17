package lex.hack.game.entities;

import lex.hack.game.Game;

public abstract class EntityLiving extends Entity {
	public double hp, maxhp, size;
	
	public EntityLiving(Game game, double x, double y, double hp, double size) {
		super(game, x, y);
		
		this.hp = hp;
		this.maxhp = hp;
		this.size = size;
	}
	
	public void damage(double damage) {
		hp -= damage;
		if(hp < 0) {
			die();
		}
	}
	
	public void heal(double heal) {
		hp += heal;
		if(hp > maxhp) hp = maxhp;
	}
	
	public void die() {
		game.entities.remove(this);
	}
}
