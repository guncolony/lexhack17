package lex.hack.game.entities;

import java.util.HashSet;

import lex.hack.game.Game;
import lex.hack.game.blocks.Block;
import lex.hack.game.tiles.TileEntity;

public abstract class Projectile extends EntityLiving {
	double vx, vy;
	EntityLiving shooter;
	
	public Projectile(Game game, double x, double y, double hp, double size, EntityLiving shooter, double vx, double vy) {
		super(game, x, y, hp, size);
		this.shooter = shooter;
		this.vx = vx;
		this.vy = vy;
	}
	
	public static ProjBullet fireBullet(Game game, EntityLiving shooter, double x, double y, double destX, double destY, double dmg) {
		double xdist = destX-x, ydist = destY-y;
		double dist = Math.sqrt(xdist*xdist+ydist*ydist);
		double vx = xdist/dist*0.3, vy = ydist/dist*0.3;
		ProjBullet bullet = new ProjBullet(game, x, y, dmg, shooter, vx, vy);
		game.entities.add(bullet);
		return bullet;
	}
	
	@Override
	public void tick() {
		@SuppressWarnings("unchecked")
		HashSet<Entity> ents = (HashSet<Entity>) game.entities.clone();
		for(Entity re : ents) {
			if(re != shooter && re instanceof EntityLiving && !(re instanceof Projectile)) {
				EntityLiving e = (EntityLiving) re;
				// Projectile hit
				if((x - e.x) * (x - e.x) + (y - e.y) * (y - e.y) <= (size + e.size) * (size + e.size)) {
					onHit(e);
					break;
				}
			}
		}
		
		// Check passthru
		if(game.isSolid((int) x, (int) y)) {
			Block[] blocks = game.getBlocks((int) x, (int) y);
			for(int i = 0; i < 3; i++) {
				if(blocks[i] == null) {
					this.die();
					return;
				}
				if(blocks[i].collideable) {
					onCollision(blocks[i]); break;
				}
			}
		}
		
		x += vx;
		y += vy;
	}
	
	public void onHit(EntityLiving e) {
		e.damage(hp);
		this.die();
	}
	
	public void onCollision(Block b) {
		if(b instanceof TileEntity) {
			TileEntity te = (TileEntity) b;

			te.damage(hp);
		}
		this.die();
	}
	
	@Override
	public void render() {
		game.main.stroke(255, 255, 0);
		game.main.fill(200, 200, 0);
		game.main.ellipse(game.scrX(x), game.scrY(y), game.scr(size), game.scr(size));
	}
	
}
