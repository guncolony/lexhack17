package lex.hack.game.entities;

import lex.hack.game.Game;

public class ProjBullet extends Projectile {

	public ProjBullet(Game game, double x, double y, double hp, EntityLiving shooter, double vx, double vy) {
		super(game, x, y, hp, 0.4, shooter, vx, vy);
	}
	
	public void render() {
		game.main.strokeWeight(game.scr(0.05));
		game.main.stroke(255, 255, 127);
		game.main.line(game.scrX(x), game.scrY(y), game.scrX(x+vx*2), game.scrY(y+vy*2));
	}

}
