package lex.hack.game.tiles;

import lex.hack.game.Scene;
import lex.hack.game.blocks.Block;

public abstract class TileEntity extends Block {
	
	Scene scene;
	
	int x; int y; int z;
	
	double health = 0;
	double maxhealth = 0;

	public TileEntity(Scene scene, int x, int y) {
		this.scene = scene;
		this.x = x;
		this.y = y;
	}
	
	public void damage(double damage) {
		health -= damage;
		if(health < 0) {
			destroy();
		}
	}
	
	/**
	 * Destroy this tile entity.
	 */
	public void destroy() {
		scene.setBlock(x, y, z, null);
	}
	
}
