package lex.hack.game;

import lex.hack.game.blocks.*;
import processing.core.PApplet;
import processing.core.PShape;

/**
 * A game scene (level, etc)
 * <p>
 * Width of all scenes is 32 blocks.
 * @author Jason
 *
 */
public class Scene {
	public Game game;
	public int starty;
	public int height;

	public Block[][][] blocks;
	public PShape shapeGroup;
	
	public Scene(Game game, int starty, int height) {
		this.game = game;
		this.starty = starty;
		this.height = height;
		shapeGroup = new PShape(PApplet.GROUP);
		blocks = new Block[32][height][3];
		for(int x = 0; x < 32; x++) {
			for(int y = 0; y < height; y++) {
				for(int z = 0; z < 3; z++) {
					blocks[x][y][z] = initializeBlock(x, y, z);
				}
			}
		}
	}
	
	public Block initializeBlock(int x, int y, int z) {
		if(z == 0) return Blocks.GRASS;
		else return null;
	}
	
	/**
	 * Returns the height of the scene, in blocks.
	 * @return
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Renders all blocks on the screen.
	 */
	public void drawBlocks() {
		for(int x = 0; x < 32; x++) {
			for(int y = 0; y < height; y++) {
				for(int z = 0; z < 3; z++) {
					Block block = blocks[x][y][z];
					if(block != null) {
						game.renderBlock(game.scrX(x+0.5), game.scrY(starty + y+0.5), game.scr(1), 0, block.getImage(), block.getColor());
					}
				}
			}
		}
	}
	
	/**
	 * Gets the pile of blocks at this specific coordinate
	 * @param x
	 * @param y
	 * @return
	 */
	public Block[] getBlocks(int x, int y) {
		if(x < 0 || x >= 32 || y < 0 || y >= height) {
			return null;
		}
		return blocks[x][y];
	}
	
	/**
	 * Gets the block at this specific coordinate
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public Block getBlock(int x, int y, int z) {
		if(x < 0 || x >= 32 || y < 0 || y >= height || z < 0 || z >= 3) {
			return null;
		}
		return blocks[x][y][z];
	}
	
	/**
	 * Sets the block at this specific coordinate
	 * @param x
	 * @param y
	 * @param z
	 * @param bl
	 */
	public void setBlock(int x, int y, int z, Block bl) {
		blocks[x][y][z] = bl;
	}
}
